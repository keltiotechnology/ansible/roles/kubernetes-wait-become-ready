# Kubernetes wait become ready

Wait for the cluster to become ready.

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubernetes_cluster_path_kubeconfig | Path where the cluster kubeconfig will be created | `string` | `kubeconfig.yml` | no |
